## [3.3.1](https://gitee.com/tangguoxiong/eslint-config-powerful/compare/v3.3.0...v3.3.1) (2024-09-18)


### Features

* 禁用 @typescript-eslint/no-unused-expressions ([6dd5446](https://gitee.com/tangguoxiong/eslint-config-powerful/commits/6dd54461ff17773814161b9c01b5732b28e5e6ef))



# [3.2.0](https://gitee.com/tangguoxiong/eslint-config-powerful/compare/v3.1.1...v3.2.0) (2024-04-14)


### Build System

* 更新依赖, 适配flat格式配置 ([9d08b99](https://gitee.com/tangguoxiong/eslint-config-powerful/commits/9d08b99a2fc6a2a269f7be9055fa47601c21895d))


### BREAKING CHANGES

* Node.js>=18.20.1



## [3.1.1](https://gitee.com/tangguoxiong/eslint-config-powerful/compare/v3.1.0...v3.1.1) (2024-02-02)


### Bug Fixes

* **js规则:** 移出files过滤, 应用到所有地方 ([ea809eb](https://gitee.com/tangguoxiong/eslint-config-powerful/commits/ea809eb6832ca4b23b0519d43571de7c72fcaa94))


### Features

* 新增.editorconfig配置 ([fb07013](https://gitee.com/tangguoxiong/eslint-config-powerful/commits/fb07013958f16e91f692db3d27100c0f206d80be))



# [3.1.0](https://gitee.com/tangguoxiong/eslint-config-powerful/compare/v3.0.3...v3.1.0) (2024-02-02)


### Code Refactoring

* 适配eslint平面配置格式 ([2d0ee1c](https://gitee.com/tangguoxiong/eslint-config-powerful/commits/2d0ee1c5a4217fdebdc68c57f92da79e96f112c1))


### BREAKING CHANGES

* 适配eslint平面配置格式



