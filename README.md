# eslint-config-powerful

#### 介绍

eslint共享配置(Vue3)

#### 安装教程

```shell
npm i -D eslint-config-powerful@3
```

#### 使用说明

添加 eslint.config.js 文件到项目根目录

```js
module.exports = (async () => {
	const {default:configs} = await import('eslint-config-powerful');
	return [
		{
			ignores:[
				'lib/',
			],
		},
		...configs,
		{
			files:[
				'**/powerful.config.ts',
			],
			rules:{
				'@typescript-eslint/no-unused-vars':'off',
			},
		},
	];
})();
```
